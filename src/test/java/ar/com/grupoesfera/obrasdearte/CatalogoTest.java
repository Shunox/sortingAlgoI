package ar.com.grupoesfera.obrasdearte;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class CatalogoTest {
    Catalogo catalogo;
    String autor;
    String titulo;
    double dimension;
    @Before
    public void setup(){
        catalogo = new Catalogo();
        autor = "pepe";
        titulo = "piepie";
        dimension = 10.00;
        ObraDeArte pintura = new Pintura(autor, titulo, dimension);
        ObraDeArte escultura = new Escultura(autor, titulo, dimension);
        ObraDeArte grabado = new Grabado(autor, titulo, dimension);
        catalogo.agregarObra(pintura);
        catalogo.agregarObra(escultura);
        catalogo.agregarObra(grabado);
    }

    @Test
    public void obrasDeArte(){
        assertThat(catalogo.listar()).isEqualTo("[piepie, piepie, piepie]");
    }

}
