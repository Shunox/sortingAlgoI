package ar.com.grupoesfera.obrasdearte;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class GrabadoTest {
    String autor = "Juan";
    String titulo = "CuadrosChorreantes";
    double dimension = 10;

    Grabado grabado;

    @Before
    public void setup(){
        grabado =  new Grabado(autor, titulo, dimension);
    }

    @Test
    public void autor(){
        assertThat(grabado.getAutor()).isEqualTo(autor);
    }
    @Test
    public void titulo(){
        assertThat(grabado.getTitulo()).isEqualTo(titulo);
    }
    @Test
    public void dimension(){
        assertThat(grabado.getDimensiones()).isEqualTo(dimension);
    }
}
