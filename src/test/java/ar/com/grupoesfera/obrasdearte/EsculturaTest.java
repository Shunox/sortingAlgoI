package ar.com.grupoesfera.obrasdearte;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class EsculturaTest {
    String autor = "Juan";
    String titulo = "CuadrosChorreantes";
    double dimension = 10;

    Escultura escultura;

    @Before
    public void setup(){
        escultura =  new Escultura(autor, titulo, dimension);
    }

    @Test
    public void autor(){
        assertThat(escultura.getAutor()).isEqualTo(autor);
    }
    @Test
    public void titulo(){
        assertThat(escultura.getTitulo()).isEqualTo(titulo);
    }
    @Test
    public void dimension(){
        assertThat(escultura.getDimensiones()).isEqualTo(dimension);
    }
}
