package ar.com.grupoesfera.obrasdearte;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class PinturaTest {
    String autor = "Juan";
    String titulo = "CuadrosChorreantes";
    double dimension = 10;

    Pintura pintura;

    @Before
    public void setup(){
        pintura =  new Pintura(autor, titulo, dimension);
    }

    @Test
    public void autor(){
        assertThat(pintura.getAutor()).isEqualTo(autor);
    }
    @Test
    public void titulo(){
        assertThat(pintura.getTitulo()).isEqualTo(titulo);
    }
    @Test
    public void dimension(){
        assertThat(pintura.getDimensiones()).isEqualTo(dimension);
    }
}
