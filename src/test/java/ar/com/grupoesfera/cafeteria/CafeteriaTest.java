package ar.com.grupoesfera.cafeteria;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class CafeteriaTest {

    Taza taza;
    Cafe cafe;
    Azucar azucar;
    Leche leche;
    Canela canela;
    Cafeteria cafeteria;
    @Before
    public void setup(){
        cafe = new Cafe();
        azucar = new Azucar();
        leche = new Leche();
        canela = new Canela();
        taza = new Taza();
        cafeteria = new Cafeteria();

        cafeteria.stock.put(cafe,500);
        cafeteria.stock.put(azucar,500);
        cafeteria.stock.put(leche,500);

    }
    @Test
    public void crearTaza() throws Exception {
        taza.agregarIngrediente(cafe, 200);
        taza.agregarIngrediente(azucar, 300);
        taza.agregarIngrediente(leche, 50);

        cafeteria.crearTaza();
        cafeteria.agregarIngrediente(cafe, 200);
        cafeteria.agregarIngrediente(azucar, 300);
        cafeteria.agregarIngrediente(leche, 50);

        assertThat(cafeteria.obtenerTaza()).isEqualTo(taza);
    }

    @Test (expected = Exception.class)
    public void agregarIngredienteFueraDeStock() throws Exception {

        cafeteria.crearTaza();
        cafeteria.agregarIngrediente(cafe, 600);

    }



}
