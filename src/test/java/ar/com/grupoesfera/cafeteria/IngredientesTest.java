package ar.com.grupoesfera.cafeteria;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class IngredientesTest {
    Cafe cafe;
    Azucar azucar;
    Leche leche;
    Canela canela;

    @Before
    public void setup(){
        cafe = new Cafe(150);
        azucar = new Azucar(500);
        leche = new Leche(200);
        canela = new Canela(300);
    }

    @Test
    private void verificarPrecios(){
        assertThat(cafe.getPrecio()).isEqualTo(150);
        assertThat(azucar.getPrecio()).isEqualTo(500);
        assertThat(leche.getPrecio()).isEqualTo(200);
        assertThat(canela.getPrecio()).isEqualTo(300);
    }

}
