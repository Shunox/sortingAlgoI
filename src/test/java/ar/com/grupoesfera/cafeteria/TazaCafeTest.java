package ar.com.grupoesfera.cafeteria;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;


public class TazaCafeTest {

    Taza taza;
    Cafe cafe;
    Azucar azucar;
    Leche leche;
    Canela canela;

    @Before
    public void setup(){
        taza = new Taza();
        cafe = new Cafe(150);
        azucar = new Azucar(500);
        leche = new Leche(200);
        canela = new Canela(300);
    }
    @Test
    public void verificarIngredientes() throws Exception {
        taza.agregarIngrediente(cafe, 200);
        taza.agregarIngrediente(azucar, 300);
        taza.agregarIngrediente(leche, 50);

        assertThat(taza.ingredientes.get(0).getValue0()).isEqualTo(cafe);
        assertThat(taza.ingredientes.get(0).getValue1()).isEqualTo(200);
        assertThat(taza.ingredientes.get(1).getValue0()).isEqualTo(azucar);
        assertThat(taza.ingredientes.get(1).getValue1()).isEqualTo(300);
        assertThat(taza.ingredientes.get(2).getValue0()).isEqualTo(leche);
        assertThat(taza.ingredientes.get(2).getValue1()).isEqualTo(50);

    }

    @Test
    public void agregarDosVecesIngrediente() throws Exception {
        taza.agregarIngrediente(cafe, 200);
        taza.agregarIngrediente(cafe, 400);
        taza.agregarIngrediente(azucar, 300);
        taza.agregarIngrediente(leche, 50);

        assertThat(taza.ingredientes.get(0).getValue0()).isEqualTo(cafe);
        assertThat(taza.ingredientes.get(0).getValue1()).isEqualTo(600);
        assertThat(taza.ingredientes.get(1).getValue0()).isEqualTo(azucar);
        assertThat(taza.ingredientes.get(1).getValue1()).isEqualTo(300);
        assertThat(taza.ingredientes.get(2).getValue0()).isEqualTo(leche);
        assertThat(taza.ingredientes.get(2).getValue1()).isEqualTo(50);

    }

    @Test (expected = Exception.class)
    public void verificarIngredientesNegativo() throws Exception {
        taza.agregarIngrediente(cafe, -200);

    }
        @Test
    public void calcularCosto() throws Exception{
        taza.agregarIngrediente(cafe,200);
        taza.agregarIngrediente(azucar,300);
        taza.agregarIngrediente(leche,50);

        int costo=0;
        for(int i=0; i<taza.ingredientes;i++){
            costo += taza.ingredientes[i].getValue0().getPrecio() * (taza.ingredientes[i].getValue1() /100);
        }

        assertThat(taza.obtenerCosto()).isEqualTo(costo);
    }

}
