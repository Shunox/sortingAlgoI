package ar.com.grupoesfera.banco;

import org.junit.Before;
import java.util.List;
import org.junit.*;
import static org.assertj.core.api.Assertions.*;


public class CuentaAhorroTest {
    Cliente c1;
    CuentaAhorro cuenta;

    @Before
    public void initialize(){
        String nombre = "Federico";
        String apellido = "Gomez";
        String username = "asd";
        c1 = new Cliente(nombre, apellido, username);
        cuenta = new CuentaAhorro(c1);

    }

    // a sueldo extraer mas de 5 veces son 15 p mas

    @Test
    public void extraer() throws Exception{
        cuenta.extraer(100);
        cuenta.extraer(100);
        cuenta.extraer(100);
        cuenta.extraer(100);
        cuenta.extraer(100);
        cuenta.extraer(100);
        assertThat(cuenta.getSaldo()).isEqualTo(400);

    }

    @Test
    public void depositar() throws Exception{
        cuenta.depositar(300);
        assertThat(cuenta.getSaldo()).isEqualTo(1300);
    }

    @Test(expected = Exception.class)
    public void depositarNegativo() throws Exception{
        cuenta.depositar(-1);

    }

    @Test(expected = Exception.class)
    public void extraerNegativo() throws Exception{
        cuenta.extraer(-1);
    }
}
