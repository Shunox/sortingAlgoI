package ar.com.grupoesfera.banco;

import org.junit.Before;
import java.util.List;
import org.junit.*;
import static org.assertj.core.api.Assertions.*;

public class CuentaAhorroEspecialTest {
    Cliente c1;
    CuentaAhorroEspecial cuenta;

    @Before
    public void initialize(){
        String nombre = "Federico";
        String apellido = "Gomez";
        String username = "asd";
        c1 = new Cliente(nombre, apellido, username);
        cuenta = new CuentaAhorroEspecial(c1, 10);

    }

    @Test
    public void extraerDelDescubierto() throws Exception{
        cuenta.extraer(1001);
        assertThat(cuenta.getSaldo()).isEqualTo(0);
        assertThat(cuenta.getDescubierto()).isEqualTo(9);
    }

    @Test(expected = Exception.class)
    public void  extraerMasQueElDescubierto() throws Exception{
        cuenta.extraer(1011);

    }
}
