package ar.com.grupoesfera.banco;

import java.util.List;
import org.junit.*;
import static org.assertj.core.api.Assertions.*;

public class ClienteTest {
    String nombre = "Federico";
    String apellido = "Antonacci";
    String username = "piepie";
    Cliente cliente = new Cliente("Federico", "Antonacci", "piepie");

    @Test
    public void  pedirNombre(){
        assertThat(cliente.getNombre()).isEqualTo(nombre);

    }
    @Test
    public void pedirApellido(){
        assertThat(cliente.getApellido()).isEqualTo(apellido);
    }
    @Test
    public void  pedirUsuario(){
        assertThat(cliente.getUser()).isEqualTo(username);
    }




}
