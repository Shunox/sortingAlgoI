package ar.com.grupoesfera.alarma;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class sensorTest {

    /*private boolean activado;
    private boolean encendido;

    public Sensor(){
        activado = false;
        encendido = true;
    }
    */
    public Sensor sTest;
    @Before
    public void setup(){
        sTest = new Sensor();
    }

    @Test(expected = Exception.class)
    public void encenderEncendido() throws Exception{
        sTest.encender();
        sTest.encender();
    }

    @Test(expected = Exception.class)
    public void apagarApagado() throws Exception{
        sTest.apagar();
    }

    @Test(expected = Exception.class)
    public void activarApagado() throws Exception{
        sTest.activar();
    }


    @Test(expected = Exception.class)
    public void activarActivado() throws Exception{
        sTest.encender();
        sTest.activar();
        sTest.activar();
    }

    @Test
    public void activar() throws Exception{
        sTest.encender();
        assertThat(sTest.activado()).isFalse();
        sTest.activar();
        assertThat(sTest.activado()).isTrue();
    }


}
