package ar.com.grupoesfera.alarma;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class alarmaTest {

    public Alarma alarma;
    @Before
    public void setup(){
        alarma = new Alarma(1234);
    }

    @Test(expected = Exception.class)
    public void encenderEncendido() throws Exception{
        alarma.encender();
        alarma.encender();
    }

    @Test(expected = Exception.class)
    public void apagarClaveIncorrecta() throws Exception{
        alarma.encender();
        alarma.apagar(111);
    }

    @Test(expected = Exception.class)
    public void apagarApagada() throws Exception{
        alarma.apagar(1234);
    }

    @Test
    public void apagar() throws Exception{
        alarma.encender();
        alarma.apagar(1234);
        assertThat(true).isTrue();

    }

    @Test
    public void activada() throws Exception{
        assertThat(alarma.activada()).isFalse();
        alarma.encender();
        alarma.sSonido.activar();
        alarma.sContacto.activar();
        alarma.sMovimiento.activar();
        assertThat(alarma.activada()).isTrue();
    }
}
