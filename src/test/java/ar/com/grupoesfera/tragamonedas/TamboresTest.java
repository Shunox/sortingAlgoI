package ar.com.grupoesfera.tragamonedas;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class TamboresTest {

    public Tambor tambor1;
    public List<Integer> posiciones;

    @Before
    public void Initialize(){
        tambor1 = new Tambor();
        posiciones = new ArrayList<Integer>();
        for (int i=1;i<=8;i++){
            posiciones.add(i);
        }

    }

    @Test
    public void girarTambor(){
        int posicion = tambor1.obtenerPosicion();

        for (int i=0;i<100;i++) {
            tambor1.girar();
            assertThat(tambor1.obtenerPosicion()).isIn(posiciones);
        }

    }


}

