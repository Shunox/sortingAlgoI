package ar.com.grupoesfera.tragamonedas;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.*;
public class TragamonedasTest {

    public Tragamonedas tragamonedas1;
    public List<Integer> posiciones;

    @Before
    public void Initialize(){
        tragamonedas1 = new Tragamonedas();
        posiciones = new ArrayList<Integer>();
        for (int i=1;i<=8;i++){
            posiciones.add(i);
        }

    }

    @Test
    public void activarTest(){
        int posicionTambor1 = tragamonedas1.getTambor1().obtenerPosicion();
        int posicionTambor2 = tragamonedas1.getTambor2().obtenerPosicion();
        int posicionTambor3 = tragamonedas1.getTambor3().obtenerPosicion();

        assertThat(tragamonedas1.getTambor1().obtenerPosicion()).isIn(posiciones);
        assertThat(tragamonedas1.getTambor2().obtenerPosicion()).isIn(posiciones);
        assertThat(tragamonedas1.getTambor3().obtenerPosicion()).isIn(posiciones);

        tragamonedas1.activar();

        assertThat(tragamonedas1.getTambor1().obtenerPosicion()).isIn(posiciones);
        assertThat(tragamonedas1.getTambor2().obtenerPosicion()).isIn(posiciones);
        assertThat(tragamonedas1.getTambor3().obtenerPosicion()).isIn(posiciones);

    }

    @Test
    public void entregaPremioTest(){
        int posicionTambor1;
        int posicionTambor2;
        int posicionTambor3;

        for(int i=0;i<100;i++){
            tragamonedas1.activar();
            posicionTambor1 = tragamonedas1.getTambor1().obtenerPosicion();
            posicionTambor2 = tragamonedas1.getTambor2().obtenerPosicion();
            posicionTambor3 = tragamonedas1.getTambor3().obtenerPosicion();

            if(posicionTambor1==posicionTambor2 && posicionTambor1==posicionTambor3){
                assertThat(tragamonedas1.entregaPremio()).isTrue();
            }else{
                assertThat(tragamonedas1.entregaPremio()).isFalse();
            }
        }
    }

}
