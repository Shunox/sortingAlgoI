package ar.com.grupoesfera.banco;

public class Cliente implements Notificable {

    private String nombre;
    private String apellido;
    private String user;

    public String getNombre() {
        return nombre;
    }



    public String getApellido() {
        return apellido;
    }



    public String getUser() {
        return user;
    }

    Cliente(String nombre,String apellido,String username){
        this.nombre=nombre;
        this.apellido=apellido;
        this.user=username;

    }


    @Override
    public void notificar() {
        
    }
}
