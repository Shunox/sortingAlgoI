package ar.com.grupoesfera.banco;

import java.util.HashMap;

public class CuentaAhorroMulticliente extends CuentaAhorro {

    HashMap<String,Cliente > clientes;

    CuentaAhorroMulticliente(Cliente cliente){
        super(cliente);
        clientes = new HashMap<String, Cliente>();
        clientes.put(cliente.getUser(),cliente);
    }
    public void agregarCliente(Cliente nuevoCliente) throws Exception{
        if (clientes.containsKey(nuevoCliente.getUser())){
            throw new Exception("Ya existe ese cliente");

        }
        clientes.put(nuevoCliente.getUser(),nuevoCliente);

    }
}
