package ar.com.grupoesfera.banco;

public interface Notificable {

    public void notificar();
}
