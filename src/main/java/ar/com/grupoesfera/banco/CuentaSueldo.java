package ar.com.grupoesfera.banco;

public class CuentaSueldo extends CuentaBancaria {

    private int cantidadExtracciones=0;

    public CuentaSueldo(Cliente cliente){
        super(cliente);
    }

    @Override
    public void extraer(double extraccion) throws Exception{
        if(extraccion<0){
            throw new Exception("El saldo a extraer es negativo");
        }
        double saldoNuevo = this.getSaldo() -extraccion;
        this.setSaldo(saldoNuevo);
        cantidadExtracciones++;
        if(cantidadExtracciones>5){
            saldoNuevo = this.getSaldo() -15.00;
            this.setSaldo(saldoNuevo);
            cantidadExtracciones=0;
        }

    }

}
