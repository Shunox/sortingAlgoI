package ar.com.grupoesfera.banco;

public abstract class CuentaBancaria {

    private Cliente cliente;

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    private double saldo;


    public CuentaBancaria(Cliente cliente){
        this.cliente = cliente;
        this.saldo=1000.00;
    }

    public void depositar(double income) throws Exception{
        if(income<0){
            throw new Exception("El saldo a depositar es negativo");
        }
        this.saldo+=income;
    }
    public void extraer(double extraccion) throws Exception{
        if(extraccion<0){
            throw new Exception("El saldo a extraer es negativo");
        }

        this.saldo-=extraccion;
    }


}
