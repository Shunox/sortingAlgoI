package ar.com.grupoesfera.banco;

public class CuentaAhorroEspecial extends CuentaAhorro {

    private double descubierto;

    private double descubiertoMin;

    public double getDescubiertoMin() {
        return descubiertoMin;
    }

    public void setDescubiertoMin(double descubiertoMin) {
        this.descubiertoMin = descubiertoMin;
    }


    public double getDescubierto() {
        return descubierto;
    }

    public void setDescubierto(double descubierto) {
        this.descubierto = descubierto;
    }


    CuentaAhorroEspecial(Cliente cliente, double descubierto){
        super(cliente);
        this.descubierto = descubierto;
        this.descubiertoMin = descubierto;
    }

    @Override
    public void extraer(double extracted) throws Exception{
        double newSaldo;
        if(extracted<0){
            throw new Exception("El saldo a extraer es negativo");
        }
        if(this.getSaldo()>=extracted) {
            newSaldo = this.getSaldo() - extracted;
        }else{
            if(this.getSaldo() + descubierto < extracted){
                throw new Exception("eh gato, no te queda mas frula");
            }
            newSaldo = 0;
            this.setDescubierto(descubierto - (extracted - this.getSaldo()));
        }
        this.setSaldo(newSaldo);

    }

    @Override
    public void depositar(double income) throws  Exception{
        double newSaldo;
        if(income<0){
            throw new Exception("El saldo a extraer es negativo");
        }
        if(descubiertoMin > descubierto){
            newSaldo = getSaldo() + income - (getDescubiertoMin() - getDescubierto());
            setDescubierto(descubiertoMin);
        }else{
            newSaldo = getSaldo() + income;
        }
        setSaldo(newSaldo);
    }


}
