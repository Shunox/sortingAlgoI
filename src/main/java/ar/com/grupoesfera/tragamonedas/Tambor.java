package ar.com.grupoesfera.tragamonedas;

public class Tambor{
    private int posicion;

    public Tambor() {
        posicion = 1 + (int)(Math.random() * 8);
    }

    public void girar(){
        posicion = 1 + (int)Math.random() * 8;
    }

    public int obtenerPosicion(){
        return posicion;
    }


}
