package ar.com.grupoesfera.tragamonedas;

public class Tragamonedas {

    private Tambor tambor1;
    private Tambor tambor2;
    private Tambor tambor3;



    public Tragamonedas(){
        tambor1 = new Tambor();
        tambor2 = new Tambor();
        tambor3 = new Tambor();
    }

    public Tambor getTambor1(){
        return tambor1;
    }

    public Tambor getTambor2(){
        return tambor2;
    }

    public Tambor getTambor3() {
        return tambor3;
    }

    public void activar(){
        tambor1.girar();
        tambor2.girar();
        tambor3.girar();
    }

    public boolean entregaPremio(){
        return tambor1.obtenerPosicion() == tambor2.obtenerPosicion() && tambor1.obtenerPosicion() == tambor3.obtenerPosicion();
    }


}
