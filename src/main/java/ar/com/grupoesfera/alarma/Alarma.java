package ar.com.grupoesfera.alarma;

public class Alarma {

    public Sensor sMovimiento;
    public Sensor sSonido;
    public Sensor sContacto;
    private int codigoSeguridad;

    public Alarma(int codigoSeguridad) {
        this.codigoSeguridad=codigoSeguridad;
        sMovimiento= new Sensor();
        sSonido = new Sensor();
        sContacto = new Sensor();
    }

    public void encender() throws Exception {
        sMovimiento.encender();
        sContacto.encender();
        sSonido.encender();

    }

    public void apagar(int codigoSeguridad) throws Exception {
        if(codigoSeguridad!=this.codigoSeguridad){
            throw new Exception("Codigo de Seguridad Incorrecto");
        }
        sMovimiento.apagar();
        sContacto.apagar();
        sSonido.apagar();
    }

    public boolean activada() {
        return sMovimiento.activado() || sContacto.activado() || sSonido.activado();
    }

}
