package ar.com.grupoesfera.alarma;

public class Sensor {

    private boolean encendido;
    private boolean activado;

    public Sensor() {
        encendido=false;
        activado=false;

    }

    public void encender() throws Exception {
        if(encendido){
            throw new Exception("Esta encendido");
        }
        encendido=true;
    }

    public void apagar() throws Exception {
        if(!encendido){
            throw new Exception("Esta apagado");
        }
        encendido=false;
    }

    public boolean activado() {
        return activado;
    }

    public void activar() throws Exception{
        if(activado){
            throw new Exception("Esta activado");
        }
        if(!encendido){
            throw new Exception("Esta apagado");
        }
        activado=true;

    }
}
