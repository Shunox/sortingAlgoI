package ar.com.grupoesfera.cafeteria;

public abstract class Ingrediente {
    private double precio;

    public double getPrecio(){
        return precio;
    }
    public  void setPrecio(double precio){
        this.precio = precio;
    }
}
