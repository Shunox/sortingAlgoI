package ar.com.grupoesfera.cafeteria;

import java.util.ArrayList;
import java.util.List;

public class Taza {
    public class PairIngredienteint

    {
        int right;
        Ingrediente left;

        public PairIngredienteint(Ingrediente i, int r){
            this.left = i;
            this.right = r;
        }

        public Ingrediente getValue0() {
            return left;
        }

        public void setValue0(Ingrediente left) {
            this.left = left;
        }

        public int getValue1() {
            return right;
        }

        public void setValue1(int right) {
            this.right = right;
        }



    }


     public List<PairIngredienteint> ingredientes = new ArrayList<PairIngredienteint>();

    public void agregarIngrediente (Ingrediente i, int c) throws Exception{
        if(c < 0){
            throw new Exception("no agregues negativo");
        }
        for (PairIngredienteint ingrediente : ingredientes) {
            if (ingrediente.getValue0() == i) {
                ingrediente.setValue1(ingrediente.getValue1() + c);
                return;
            }
        }
        PairIngredienteint pair = new PairIngredienteint(i, c);
        this.ingredientes.add(pair);
    }
    public double obtenerCosto(){
        double costo = 0.00
        for (PairIngredienteint ingred:ingredientes) {
            costo+= ingred.getValue0().getPrecio()*ingred.getValue1()/100;
        }
    }

}


