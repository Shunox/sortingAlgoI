package ar.com.grupoesfera.obrasdearte;

import java.util.ArrayList;
import java.util.List;

public class Catalogo {

    List<ObraDeArte> obras;

    public Catalogo(){
        obras = new ArrayList<ObraDeArte>();
    }

    public void agregarObra(ObraDeArte obra){
        obras.add(obra);
    }

    public String listar(){
        String lista="[";

        for(int i=0;i<obras.size()-1;i++){
            lista += obras.get(i).toString() + ", ";
        }
        lista+= obras.get(obras.size()-1).toString() + "]";

        return lista;
    }

}
