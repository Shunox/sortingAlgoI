package ar.com.grupoesfera.obrasdearte;

public abstract class ObraDeArte extends Object {

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public double getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(double dimensiones) {
        this.dimensiones = dimensiones;
    }

    String autor;
    String titulo;
    double dimensiones;

    @Override
    public String toString(){
        return this.getTitulo();
    }
}
